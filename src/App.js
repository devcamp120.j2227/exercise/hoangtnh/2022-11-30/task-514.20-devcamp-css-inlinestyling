import avatar from "./assets/images/avatar.jpg";

function App() {
  return (
    <div>
      <div style={{width: "800px", margin: "100px auto", padding: "30px", textAlign: "center", border: "1px solid #ddd", backgroundColor:" bisque"}}>
        <div>
          <img src={avatar} alt="Avatar" style={{borderRadius: "50%", width: "100px", marginTop: "-100px"}} />
        </div>
        <div style={{fontSize: "18px", fontStyle:"italic"}}>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div style={{fontSize: "13px", color:"brown"}}>
          <b style ={{color:"blue"}}>
            Tammy Stevens 
          </b>
          &nbsp; * &nbsp;Front End Developer
        </div>
      </div>
    </div>

  );
}

export default App;
